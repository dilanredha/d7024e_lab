package main

import (
	"fmt"
	"net"
)

type Network struct {
	//kad  *Kademlia
	addr *net.UDPAddr
	body []byte
	conn *net.UDPConn
}
type RPC struct {
	procedure string `yaml:"procedure"`
	sender    int    `yaml:"sender"`
	recipient int    `yaml:"recipient"`
}

func main() {
	Listen("localhost", 5500)

}

var (
	data = `
		procedure: PING
		sender: 1010
		recipient: 1011
		`
)

// https://pkg.go.dev/net#ListenUDP
func Listen(ip string, port int) {
	addr := net.UDPAddr{
		IP:   net.ParseIP(ip),
		Port: port,
	}
	ln, err := net.ListenUDP("udp", &addr)
	if err != nil {
		panic(err)
	}
	defer ln.Close()
	buffer := make([]byte, 2048)
	for {
		n, sender, err := ln.ReadFromUDP(buffer)
		if err != nil {
			fmt.Println(err)
			return
		}
		//kad := Kademlia.NewKademlia(sender.String())
		network := Network{
			//	kad:  kad,
			body: buffer[:n],
			addr: sender,
			conn: ln,
		}
		go network.connection()

	}
}

func (network *Network) connection() (err error) {

	//test, err = yaml.Marshal(&network.body)
	// if err != nil {
	// 	panic(err)
	// }
	fmt.Printf("packet-received: bytes=%d from=%s\n",
		len(network.body), string(network.body))

	fmt.Printf("RPC:%s \nsender=%s\nrecipient=%s\n",
		string(network.body[:1]), string(network.body[2:18]), string(network.body[19:39]))

	_, err1 := network.conn.WriteTo(network.body[:39], network.addr)
	if err1 != nil {
		return err1
	}

	fmt.Println("sent to client", network.addr.String())

	return //temp for now
}

/*

func (network *Network) SendPingMessage(contact *Contact) {
	// TODO
}

func (network *Network) SendFindContactMessage(contact *Contact) {
	// TODO
}

func (network *Network) SendFindDataMessage(hash string) {
	// TODO
}

func (network *Network) SendStoreMessage(data []byte) {
	// TODO
}
*/
