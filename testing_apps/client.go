package main

import (
	"bufio"
	"fmt"
	"net"
	"time"
)

func main() {

	conn, _ := net.Dial("udp", "localhost:5500")
	conn.Write([]byte("hello server please hold my hands\n"))
	time.Sleep(time.Millisecond * 100)
	buf, _, _ := bufio.NewReader(conn).ReadLine()
	fmt.Println("clnt recv", string(buf))
	conn.Close()
}
