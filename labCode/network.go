package d7024e

import (
	"fmt"
	"net"

	"gopkg.in/yaml.v2"
)

type Network struct {
	kad  *Kademlia
	addr *net.UDPAddr
	body RPC
	conn *net.UDPConn
}

type RPC struct {
	Procedure string `yaml:"procedure"`
	Sender    int    `yaml:"sender"`
	Recipient int    `yaml:"recipient"`
}

// https://pkg.go.dev/net#ListenUDP
func Listen(ip string, port int) {
	addr := net.UDPAddr{
		IP:   net.ParseIP(ip),
		Port: port,
	}
	ln, err := net.ListenUDP("udp", &addr)
	if err != nil {
		panic(err)
	}
	defer ln.Close()
	buffer := make([]byte, 2048)
	for {
		n, sender, err := ln.ReadFromUDP(buffer)
		if err != nil {
			fmt.Println(err)
			return
		}

		kad := NewKademlia(sender.String())
		network := Network{
			kad:  kad,
			body: buffer[:n],
			addr: sender,
			conn: ln,
		}
		go network.connection()

	}
}

func (network *Network) connection() (err error) {

	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("packet-received: bytes=%d from=%s\n",
		len(network.body), string(network.body))
	network.conn.WriteTo(network.body, network.addr)

	var rpc RPC
	marshalErr := yaml.Unmarshal(network.body, &rpc)
	if marshalErr != nil {
		panic(errmar)
	}

	switch rpc.Procedure {
	case "PING":
		network.SendPingMessage()
	case "FINDCONTACT":
		network.SendPingMessage()
	case "FINDDATA":
		network.SendPingMessage()
	case "STORE":
		network.SendPingMessage()
	default:
		fmt.Println("Malformed Procedure")
	}

	return //temp for now
}

func (network *Network) SendPingMessage(contact *Contact) {
	// TODO
	network.conn.WriteTo([]byte("PING"), network.addr)
}

func (network *Network) SendFindContactMessage(contact *Contact) {
	// TODO
}

func (network *Network) SendFindDataMessage(hash string) {
	// TODO
}

func (network *Network) SendStoreMessage(data []byte) {
	// TODO
}
