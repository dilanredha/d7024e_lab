package d7024e

type Kademlia struct {
	me *Contact
}

func NewKademlia(addr string) *Kademlia {
	cont := NewContact(NewKademliaID("FFFFFFFF00000000000000000000000000000000"), addr)
	newKad := Kademlia{me: &cont}
	return &newKad
}

func (kademlia *Kademlia) LookupContact(target *Contact) {
	// TODO
}

func (kademlia *Kademlia) LookupData(hash string) {
	// TODO
}

func (kademlia *Kademlia) Store(data []byte) {
	// TODO
}
